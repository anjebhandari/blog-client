import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import router from "./router"
import BootstrapVue from 'bootstrap-vue'
import Vuelidate from 'vuelidate'
import VueAxios from 'vue-axios'
// import axios from 'axios'
import VueAuthenticate from 'vue-authenticate'

import store from './store/store'
import axios from './assets/axios/index.js'

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(Vuelidate)
Vue.use(VueAxios, axios)
Vue.use(require('vue-moment'));

new Vue({
  store,
  render: h => h(App),
  router,
  axios
}).$mount('#app')
