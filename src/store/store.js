import Vuex from 'vuex'
import Vue from 'vue'
// import axios from '../assets/axios/index.js'
// const axios = require('axios')
import vueAuth from 'vue-authenticate'


Vue.use(Vuex)

export default new Vuex.Store({
  state: {

  },
  getters: {

  },
  mutations: {

  },
  actions: {

  }
})