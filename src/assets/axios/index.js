import axios from 'axios'

const API_URL = 'http://localhost:8000/api/'
var user_object = JSON.parse(localStorage.getItem('user')) || ''

export default axios.create({
  baseURL: API_URL,
  headers: {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer' + user_object.api_token
  }
})