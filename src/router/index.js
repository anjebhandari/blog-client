import Vue from 'vue'
import Router from 'vue-router'

const Login = () => import("@/components/auth/Index.vue")
const ArticleIndex = () => import("@/components/articles/Index.vue")
const Admin = () => import("@/components/dashboard/Admin.vue")
const Author = () => import("@/components/dashboard/Author.vue")
const Reader = () => import("@/components/dashboard/Reader.vue")
const Dashboard = () => import("@/components/dashboard/Index.vue")
const App = () => import("../../src/App.vue")
const CreateArticle = () => import("@/components/dashboard/author/Create.vue")
const Article = () => import("@/components/dashboard/author/Article.vue")
const Index = () => import('@/components/home/Index.vue')
const Single = () => import('@/components/home/Single.vue')

Vue.use(Router)

const router = new Router({
  mode: "history",
  routes: [
    {
      path: '/',
      name: 'index',
      component: Index
    },
    {
      path: '/home/article/:articleId',
      name: 'single',
      component: Single,
      props: true
    },
    {
      path: "/login",
      name: "login",
      component: Login


    },
    {
      path: '/home',
      name: 'artice',
      component: ArticleIndex
    }, {
      path: '/dashboard',
      name: "dashboard",
      component: Dashboard,

    },
    {
      path: "/admin",
      name: "admin",
      component: Admin
    },
    {
      path: "/author",
      name: "author",
      component: Author
    },
    {
      path: "/reader",
      name: "reader",
      component: Reader,
    },
    {
      path: "/addArticle",
      name: "createArticle",
      component: CreateArticle
    },
    {
      path: "/article/:id",
      name: "singleArticle",
      component: Article,
      props: true
    }
  ]
})

export default router;